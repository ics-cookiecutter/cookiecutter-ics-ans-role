#!/usr/bin/env python
import os
import shutil

PROJECT_DIRECTORY = os.getcwd()


def remove_file(filepath):
    os.remove(os.path.join(PROJECT_DIRECTORY, filepath))


def move_files(src, dst):
    names = os.listdir(src)
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        os.rename(srcname, dstname)


if __name__ == '__main__':
    if '{{ cookiecutter.has_dependencies }}' != 'y':
        remove_file('molecule/default/requirements.yml')
    molecule_driver = '{{ cookiecutter.molecule_instance.split()[0] }}'
    driver_src = 'molecule/default/{}'.format(molecule_driver)
    move_files(driver_src, 'molecule/default')
    for driver in ('docker', 'vagrant'):
        shutil.rmtree('molecule/default/{}'.format(driver))
    if '{{ cookiecutter.extra_inventory }}' == 'none':
        shutil.rmtree('molecule/default/inventory')
    elif '{{ cookiecutter.extra_inventory }}' == 'dummy':
        remove_file('molecule/default/inventory/csentry')
    elif '{{ cookiecutter.extra_inventory }}' == 'csentry':
        remove_file('molecule/default/inventory/dummy')
