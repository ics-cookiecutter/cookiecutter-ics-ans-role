Cookiecutter Ansible Role
=========================

Cookiecutter_ template for ESS ICS Ansible_ role: ics-ans-role-<name>

Quickstart
----------

Install the latest Cookiecutter if you haven't installed it yet::

    $ pip install cookiecutter

Generate an Ansible role project::

    $ cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-ics-ans-role.git

As this is not easy to remember, you can add an alias in your *~/.bash_profile*::

    alias new-role='cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-ics-ans-role.git'

Detailed instructions
---------------------

The role will be named `ics-ans-role-<name>`. `ics-ans-role` is automatically prepended.
Only enter `<name>` when asked for the role_name.

To create `ics-ans-role-foo`::

    $ new-role
    company [European Spallation Source ERIC]:
    full_name [Anders Harrisson]:
    role_name [ics-ans-role-<name>]: foo
    description [Ansible role to install foo]:
    has_dependencies [n]: y
    Select molecule_driver:
    1 - centos:7 docker
    2 - centos:7 docker - with systemd
    3 - vagrant centos/7
    4 - ubuntu:22.04 docker
    5 - ubuntu:22:04 docker - with systemd
    6 - vagrant ubuntu/jammy64
    7 - vagrant juniper/vqfx10k-re
    Choose from 1, 2 [1]:
    Select extra_inventory:
    1 - none
    2 - dummy
    3 - csentry
    Choose from 1, 2, 3 [1]:

This creates the following project::

    ics-ans-role-foo/
    ├── LICENSE
    ├── README.md
    ├── defaults
    │   └── main.yml
    ├── meta
    │   └── main.yml
    ├── molecule
    │   └── default
    │       ├── molecule.yml
    │       ├── converge.yml
    │       ├── prepare.yml
    │       ├── requirements.yml
    │       └── tests
    │           └── test_default.py
    └── tasks
        └── main.yml

The project includes required files to test the role using Molecule_.

If you choose to use an extra inventory, a directory named `molecule/default/inventory` will be created.
For *dummy* hosts, you can add extra hosts to the `molecule/default/inventory/dummy` file (ini style).
If you select *csentry*, the csentry-inventory script will be used. You have to export the **CSENTRY_URL** and **CSENTRY_TOKEN** environment variables.
In both cases, the playbook only runs on hosts part of the *molecule_group*. Make sure that all molecule instances you create are part of that group.
Or modify the playbook to target other hosts.


License
-------

BSD 2-clause license

.. _Ansible: http://docs.ansible.com/ansible/
.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _Molecule: http://molecule.readthedocs.io/en/master/
